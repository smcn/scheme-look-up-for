(define-module (smcn look-up-for)
  #:export (look-up-for))

;; LOOK-UP-DIR takes two variables: CHILD and CUR-DIR, both strings.
;;
;; LOOK-UP-DIR will return #f if the CHILD isn't found, otherwise it'll return
;; the absolute path to the CHILD.
(define look-up-for
  (lambda (child cur-dir)
    (if (string=? cur-dir "") #f
	(let ([dir-port (opendir cur-dir)])
	  (if (search-dir child dir-port) (string-append cur-dir "/" child)
	      (look-up-for child (go-up-dir cur-dir)))))))

(define search-dir
  (lambda (needle dir-port)
    (let ((child (readdir dir-port)))
      (if (eof-object? child) #f
	  (if (string=? needle child) #t
	      (search-dir needle dir-port))))))

(define go-up-dir
  (lambda (path)
    (string-join (reverse (cdr (reverse (string-split path #\/)))) "/")))
